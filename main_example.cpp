int main(int argc, char *argv[])
{
    try
    {
        plog::init(plog::debug, "some/path.txt");
        static plog::MyAppender<plog::FuncMessageFormatter> myAppender;
        plog::init<UsersLog>(plog::debug, &myAppender);

        qmlRegisterType<SwiftStorageModel>("com.educ", 1, 0, "SwiftStorageModel");
        qmlRegisterType<DragNDropFileSystemModel>("com.educ", 1, 0, "DragNDropFileSystemModel");

        Keychainstorage keyChainStorage;
        ServerManager serverManager(&keyChainStorage);
        TaskManager taskManager;

        QQmlApplicationEngine engine;
        QQmlContext* context = engine.rootContext();

        context->setContextProperty("CustomPLogAppender", myAppender.getRecordsModel());
        context->setContextProperty("TaskManager", taskManager.getTasks());
        context->setContextProperty("ServerManager", &serverManager);

        engine.load(QUrl(QLatin1String(qmlMainPath)));

        return app.exec();
    }
    catch (const std::exception& exp)
    {
        qFatal("%s", exp.what());
    }
}
